﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_4
{
    public class PlantDistribution
    {
        public List<string> Areas { get; set; }
        public List<Plant> PlantDataSet { get; set; }
        public PlantDistribution()
        {
            Areas = new List<string>();
            PlantDataSet = new List<Plant>();
        }
    }
    public class Plant
    {
        public string Name { get; set; }
        public List<double> Areas { get; set; }
        public Plant()
        {
            Areas = new List<double>();
        }
    }
    public class IO
    {
        public static PlantDistribution Read(string csvFile)
        {
            PlantDistribution pd = new PlantDistribution();
            StreamReader sr = new StreamReader(csvFile);
            string[] headers = sr.ReadLine().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            pd.Areas = headers.ToList();
            int n = 0;
            while (!sr.EndOfStream)
            {
                Plant p = new Plant();
                string[] distribs = sr.ReadLine().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                p.Name = (n++).ToString();
                for (int i=0; i< distribs.Length; i++)
                {
                    p.Areas.Add(double.Parse(distribs[i]));
                }
                pd.PlantDataSet.Add(p);
            }
            sr.Close();
            return pd;
        }
    }
}




















