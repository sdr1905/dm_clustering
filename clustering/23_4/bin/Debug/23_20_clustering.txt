Clusters:	20
Precision:	2.21097725769817%
		Cluster: 0
			Instance: 1621
			Area: 66
			co, ca, ga, ct, pr, lb, tx, la, tn, pa, pe, mn, de, dc, hi, me, md, ma, mb, ut, mo, fraspm, mi, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 1
			Instance: 1018
			Area: 68
			co, dengl, ca, ga, ct, pr, lb, tx, la, tn, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, fraspm, mi, nu, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 2
			Instance: 894
			Area: 57
			co, ca, ct, pr, tx, la, pe, mn, hi, yt, me, md, ma, mb, ut, mo, mi, nu, mt, qc, ms, ab, vi, ak, al, ar, vt, il, in, ia, az, id, ks, nm, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 3
			Instance: 3930
			Area: 59
			co, ca, ga, ct, pr, lb, tx, la, tn, pa, pe, mn, de, dc, hi, me, md, ma, mb, ut, mo, mi, mt, qc, ms, va, ab, vi, al, ar, vt, il, in, ia, az, ks, nj, nm, nc, nd, ne, ny, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 4
			Instance: 2822
			Area: 67
			co, ca, ga, ct, pr, lb, tx, la, tn, pa, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, fraspm, mi, nu, mt, qc, ms, va, ab, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 5
			Instance: 1216
			Area: 43
			co, ca, ga, ct, pr, tx, la, tn, pe, dc, hi, md, ma, ut, mo, mi, mt, ms, va, vi, ak, al, ar, il, az, id, ks, nj, nm, nc, ny, nv, wa, bc, wi, wv, fl, on, ok, oh, sc, ky, or
		Cluster: 6
			Instance: 2511
			Area: 39
			ca, ga, ct, pr, tx, la, tn, pe, de, dc, hi, md, ma, ut, mo, fraspm, mi, ms, va, vi, al, ar, il, az, nh, nj, nm, nb, nc, nf, ny, ns, wi, wv, ok, oh, sc, ky, or
		Cluster: 7
			Instance: 5829
			Area: 65
			co, dengl, ga, ct, lb, la, tn, pa, pe, mn, de, dc, yt, me, md, ma, mb, ut, mo, fraspm, mi, nu, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 8
			Instance: 641
			Area: 58
			co, dengl, ca, ga, ct, lb, tx, tn, pe, mn, de, hi, yt, me, md, ma, mb, ut, mo, fraspm, mi, nu, mt, qc, va, ab, ak, vt, il, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, wy, on, oh, sk, sc, ky, or, sd
		Cluster: 9
			Instance: 663
			Area: 68
			co, dengl, ca, ga, ct, pr, lb, tx, la, tn, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, fraspm, mi, nu, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 10
			Instance: 995
			Area: 67
			co, ca, ga, ct, pr, lb, tx, la, tn, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, fraspm, mi, nu, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 11
			Instance: 576
			Area: 67
			co, dengl, ca, ga, ct, pr, lb, tx, la, tn, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, fraspm, mi, nu, mt, qc, ms, va, ab, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 12
			Instance: 604
			Area: 61
			co, ca, ga, ct, pr, tx, la, tn, pe, mn, de, dc, hi, me, md, ma, mb, ut, mo, mi, mt, qc, ms, va, ab, vi, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 13
			Instance: 653
			Area: 66
			co, ca, ga, ct, pr, lb, tx, la, tn, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, fraspm, mi, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 14
			Instance: 1411
			Area: 67
			co, ca, ga, ct, pr, lb, tx, la, tn, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, fraspm, mi, nu, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 15
			Instance: 2328
			Area: 59
			co, ca, ga, ct, pr, tx, la, tn, pa, pe, mn, de, dc, hi, yt, me, md, ma, ut, mo, mi, mt, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, ks, nj, nm, nb, nc, nd, ne, ny, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, or, sd
		Cluster: 16
			Instance: 4857
			Area: 62
			co, ca, ga, ct, pr, tx, la, tn, pa, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, mi, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, ny, ns, nt, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, sd
		Cluster: 17
			Instance: 546
			Area: 68
			co, dengl, ca, ga, ct, pr, lb, tx, la, tn, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, fraspm, mi, nu, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 18
			Instance: 948
			Area: 66
			co, ca, ga, ct, pr, lb, tx, la, tn, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, mi, nu, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
		Cluster: 19
			Instance: 718
			Area: 69
			co, dengl, ca, ga, ct, pr, lb, tx, la, tn, pa, pe, mn, de, dc, hi, yt, me, md, ma, mb, ut, mo, fraspm, mi, nu, mt, qc, ms, va, ab, vi, ak, al, ar, vt, il, in, ia, az, id, nh, ks, nj, nm, nb, nc, nd, ne, nf, ny, ns, nt, ri, nv, wa, bc, wi, wv, fl, wy, on, ok, oh, sk, sc, ky, or, sd
