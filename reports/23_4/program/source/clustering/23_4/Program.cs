﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_4
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var parameters = ParseParameters(args);
                if (parameters.Count != 3)
                {
                    PrintMenu();
                    return;
                }
                string inputFile = parameters.First(x => x.Key == "-input").Value;
                int k = int.Parse(parameters.First(x => x.Key == "-k").Value);
                // default precision digits
                int algPrecision = 2;

                if (parameters.ContainsKey("-precisionDigits"))
                {
                    algPrecision = int.Parse(parameters.First(x => x.Key == "-precisionDigits").Value);
                }
                Out("Reading data...");
                PlantDistribution plantDistrib = IO.Read(inputFile);
                KMeans algorithm = new KMeans();
                KMeans.DISTANCE_PRECISION = algPrecision;

                Out("Clustering...");
                Out("Precision digits: " + algPrecision + " after dot");
                var clusters = algorithm.KmeansClustering(plantDistrib.PlantDataSet, k);

                Out("Calculate precision...");
                algorithm.RefineDistributionAreas(clusters, plantDistrib);
                var precision = algorithm.RefinePrecision(clusters, plantDistrib);
                Out("Precision: " + precision * 100 + "%");
                Out("Writing output file...");
                string outputFile = "23_" + clusters.Count + "_clustering.txt";
                WriteOutputToFile(clusters, precision, outputFile);
                Out("Result was wrote to " + Path.GetFullPath(outputFile));
                Out("DONE");
            }
            catch (Exception ex)
            {
                Out("An error occurred: \n" + ex.Message);
            }
        }
        static void WriteOutputToFile(List<PlantCluster> clusters, double precision, string outputFile)
        {
            StreamWriter sw = new StreamWriter(outputFile);
            sw.WriteLine("Clusters:\t" + clusters.Count);
            sw.WriteLine("Precision:\t" + precision * 100 + "%");
            for (int i = 0; i < clusters.Count; i++)
            {
                PlantCluster pc = clusters[i];
                sw.WriteLine("\t\tCluster: " + i);
                sw.WriteLine("\t\t\tInstance: " + pc.Plants.Count);
                sw.WriteLine("\t\t\tArea: " + pc.Areas.Count);
                string areas = string.Join(", ", pc.Areas);
                sw.WriteLine("\t\t\t" + areas);
            }
            sw.Flush();
            sw.Close();
        }
        static Dictionary<string, string> ParseParameters(string[] args)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            for (int i = 0; i < args.Length - 1; i += 2)
            {
                parameters.Add(args[i], args[i + 1]);
            }
            return parameters;
        }
        static void PrintMenu()
        {
            Out("Syntax: ");
            Out("23_4 -k <cluster> -input <input file.csv> [-precisionDigits <digits>]");
            Out("Example:\n\t23_4 -k 10 -input 23_plants.csv");
            Out("\t23_4 -k 10 -input 23_plants.csv -precisionDigits 3");
        }
        static void Out(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
