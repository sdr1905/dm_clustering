﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_4
{
    public class KMeans
    {
        public static int DISTANCE_PRECISION = 3;
        /// <summary>
        /// Calculate euclidean distance between plant p1 and plant p2
        /// </summary>
        private double GetEuclideanDistance(Plant p1, Plant p2)
        {
            double sumSquare = 0;
            for (int i = 0; i < p1.Areas.Count; i++)
            {
                double dP1 = p1.Areas[i];
                double dP2 = p2.Areas[i];
                sumSquare += Math.Pow(dP1 - dP2, 2);
            }
            return Math.Round(Math.Sqrt(sumSquare), KMeans.DISTANCE_PRECISION);
            //return sumSquare;
        }
        private List<int> RandomPosition(int k, int max)
        {
            Random r = new Random(Environment.TickCount);
            List<int> res = new List<int>();
            for (int i = 0; i < k; i++)
            {
                res.Add(r.Next(1, max));
            }
            return res;
        }
        internal class PlantClusterPosition
        {
            public Plant Plant { get; set; }
            public int OriginalCluster { get; set; }
            public int NewCluster { get; set; }
            public int IndexInCluster { get; set; }
        }
        private void RepeateClustering(List<PlantCluster> clusters, List<Plant> plantsDatSet)
        {
            List<PlantClusterPosition> outliers = new List<PlantClusterPosition>();
            Stopwatch sw = null;
            int round = 0;
            do
            {
                foreach (PlantClusterPosition pcp in outliers)
                {
                    clusters[pcp.OriginalCluster].Plants.Remove(pcp.Plant);
                }
                foreach (PlantClusterPosition pcp in outliers)
                {
                    clusters[pcp.NewCluster].Plants.Add(pcp.Plant);
                }
                outliers.Clear();
                foreach (PlantCluster cluster in clusters)
                {
                    cluster.ReCalculateMean();
                }
                sw = new Stopwatch();
                sw.Start();
                // foreach cluster 
                for (int cl = 0; cl < clusters.Count; cl++)                             // cl: cluster index
                {
                    // foreach plant p in cluster
                    for (int pidx = 0; pidx < clusters[cl].Plants.Count; pidx++)        // pidx: plant index in cluster
                    {
                        Plant p = clusters[cl].Plants[pidx];
                        double minDist = GetEuclideanDistance(p, clusters[cl].Center);
                        int clusterDestIdx = -1;

                        // foreach cluster (including current cluster), calculate distance between p and centroid 
                        for (int i = 0; i < clusters.Count; i++)
                        {
                            if (i != cl)
                            {
                                double dist = GetEuclideanDistance(p, clusters[i].Center);
                                if (minDist > dist)
                                {
                                    // min distance found, update mindist and save current cluster index
                                    minDist = dist;
                                    clusterDestIdx = i;
                                }
                            }
                        }
                        // if new cluster is the same as current cluster, skip
                        if (clusterDestIdx != -1)
                        {
                            PlantClusterPosition pcp = new PlantClusterPosition()
                            {
                                Plant = p,
                                OriginalCluster = cl,
                                IndexInCluster = pidx,
                                NewCluster = clusterDestIdx
                            };
                            outliers.Add(pcp);
                        }
                    }
                }
                sw.Stop();
                Console.WriteLine("Round " + round++ + " took " + sw.ElapsedMilliseconds + " milliseconds to refine cluster");
                Console.WriteLine("There are " + outliers.Count + " outliers");
            }
            while (outliers.Count != 0);
        }
        public List<PlantCluster> KmeansClustering(List<Plant> plantsDatSet, int k)
        {
            var centerIdxs = RandomPosition(k, plantsDatSet.Count - 1);
            List<PlantCluster> clusters = new List<PlantCluster>();
            for (int i = 0; i < k; i++)
            {
                PlantCluster pc = new PlantCluster();
                var idx = centerIdxs.ElementAt(i);
                pc.Center = plantsDatSet[idx];
                clusters.Add(pc);
                Console.WriteLine("Center " + i + " initialized at index: " + idx);
            }
            foreach (Plant p in plantsDatSet)
            {
                double minDist = double.MaxValue;
                int clusterDestIdx = -1;
                for (int i = 0; i < clusters.Count; i++)
                {
                    //if (p != clusters[i].Center)
                    //{
                    double dist = GetEuclideanDistance(p, clusters[i].Center);
                    if (minDist > dist)
                    {
                        minDist = dist;
                        clusterDestIdx = i;
                    }
                    //}
                }
                clusters[clusterDestIdx].Plants.Add(p);
            }
            RepeateClustering(clusters, plantsDatSet);
            return clusters;
        }
        public void RefineDistributionAreas(List<PlantCluster> clusters, PlantDistribution plantDistribution)
        {
            for (int c = 0; c < clusters.Count; c++)
            {
                PlantCluster cluster = clusters[c];
                if (cluster.Plants.Count > 0)
                {
                    List<double> distribArea = cluster.Plants[0].Areas.ToList();
                    for (int i = 1; i < cluster.Plants.Count; i++)
                    {
                        for (int j = 0; j < cluster.Plants[i].Areas.Count; j++)
                        {
                            distribArea[j] = (distribArea[j] + cluster.Plants[i].Areas[j]) > 0 ? 1 : 0;
                        }
                    }
                    for (int i = 0; i < distribArea.Count; i++)
                    {
                        if (distribArea[i] == 1)
                        {
                            cluster.Areas.Add(plantDistribution.Areas[i]);
                        }
                    }
                }
            }
        }
        public double RefinePrecision(List<PlantCluster> clusters, PlantDistribution plantDistribution)
        {
            int error = 0;
            // loop through all clusters
            for (int c = 0; c < clusters.Count; c++)
            {
                PlantCluster currentCluster = clusters[c];
                // loop through all plants in current cluster
                foreach (Plant plant in currentCluster.Plants)
                {
                    int appeared = 0;

                    for (int i = 0; i < plant.Areas.Count; i++)
                    {
                        // count appearance times only if this plant has a distribution in this area
                        if (plant.Areas[i] != 0)
                        {
                            var areaName = plantDistribution.Areas[i];
                            // loop throgh all cluster, count appearance 
                            for (int clidx = c + 1; clidx < clusters.Count; clidx++)
                            {
                                var cluster = clusters[clidx];
                                appeared += (cluster.Areas.FirstOrDefault(x => x.Equals(areaName, StringComparison.OrdinalIgnoreCase))) != null ? 1 : 0;
                                if (appeared > 1)
                                {
                                    break;
                                }
                            }
                        }
                        if (appeared > 1)
                        {
                            break;
                        }
                    }
                    if (appeared > 1)
                    {
                        ++error;
                    }
                }
            }
            return (double)(plantDistribution.PlantDataSet.Count - error) / plantDistribution.PlantDataSet.Count;
        }
    }
}
