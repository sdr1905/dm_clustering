﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_4
{
    public class PlantCluster
    {
        public Plant Center { get; set; }
        public List<Plant> Plants { get; set; }
        public List<string> Areas { get; set; }
        public PlantCluster()
        {
            Plants = new List<Plant>();
            Areas = new List<string>();
        }
        public void ReCalculateMean()
        {
            for (int i = 0; i < Center.Areas.Count; i++)
            {
                Center.Areas[i] = 0;
            }
            bool centerExisted = false;
            if (Plants.Count != 0)
            {
                Plants.ForEach(x =>
                {
                    if (x == Center)
                    {
                        centerExisted = true;
                    }
                    for (int i = 0; i < Center.Areas.Count; i++)
                    {
                        Center.Areas[i] += Math.Round(x.Areas[i] / Plants.Count, KMeans.DISTANCE_PRECISION);
                    }
                });
                if (!centerExisted)
                {
                    Center.Name = Guid.NewGuid().ToString();
                }
            }
        }
    }
}
